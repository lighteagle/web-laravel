<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// AuthController : register & welcome

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        $first = $request['firstname'];
        $last = $request['lastname'];
        return view('welcome',compact('first','last'));
    }
}
