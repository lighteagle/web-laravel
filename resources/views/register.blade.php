<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register - Sanber Book</title>
</head>
<body>
    <form action="/welcome" method='post'>
        @csrf
        <label>First Name:</label><br />
        <input type='text' name='firstname'><br/><br/>
        <label>Last Name:</label><br />
        <input type='text' name='lastname'><br/><br/>
        <label>Gender:</label><br />

        <input type="radio" name="gender" value="male">Male</input><br />
        <input type="radio" name="gender" value="female">Female</input><br />
        <input type="radio" name="gender" value="other">Other</input><br /><br />
        <label>Nationality:</label><br />
        <select>
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>
        <br /><br />
        <label>Language Spoken:</label><br />
        <input type="checkbox" id="bahasa1" name="bahasa1" value="indonesia">
        <label for="vehicle1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="bahasa2" name="bahasa2" value="inggris">
        <label for="vehicle1"> English</label><br>
        <input type="checkbox" id="bahasa3" name="bahasa3" value="other">
        <label for="vehicle1"> Other</label><br>
        <br /><br />
        <label>Bio:</label><br />
        <textarea rows="6" cols="50"></textarea>
        <br /><br />
        <input type='submit' value='Sign up'>
    </form>
</body>
</html>